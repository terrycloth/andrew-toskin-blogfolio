On Interstate 80, an hour's hasty drive east of Fallon, in northern Nevada ---
in the middle of nowhere --- the highway bisects a plane of white sand, freckled
with black stones, surrounded by blood-red mountains. Other than right where the
sand bulges against the edges of the road, the the lake of sand lies perfectly
flat and still. Hundreds and hundreds of previous travelers have pulled over to
gather some nearby black stones and arrange them to spell a message in the sand.
So as you drive past, you see maybe a thousand variations on:

"Mike was here"  
"Tim loves Kate"  
"J + M"  
"Gary is a fag"

Far in the north-eastern corner stands a lone 300-foot golden sand dune. I can
only guess it was man-made, because the rest of the plane is so flat, and the
dune is such a strikingly different color from anything else nearby. But what a
strange feat of engineering it must have been just to scoop together this
gigantic pile of sand. Like the white plane all around, the dune is covered in
its own little black specks, but these move. Summer is coming, and the ski
resorts are closing or closed. People crawl around like ants on an ant hill,
then sled to the bottom.
