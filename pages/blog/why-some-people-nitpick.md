# Why Some People Nitpick

Imagine you're reading a story about a carpenter who builds houses. You may or
may not know much about how houses are *built*, but you surely know a thing or
two about *houses* --- you've likely lived in one all your life.

So things are going okay till you start to notice something weird... The author
keeps mentioning doors with "12 beautiful hinges, and door knobs that press
inward at the lightest touch." You wonder why the hell these doors have so many
hinges. Also, you usually turn knobs, not push them. Weird, but you can probably
get past it, maybe it's just a story about a weird house.

But then the author mentions a window which does not open or close, and is made
of hair. Also there's a door frame hanging from the ceiling, the roof is *under*
the house, and chimney is shaped like a hook so that it releases smoke back into
the room. Surely by now you'd think What the hell, has this guy ever even seen a
house?? When you are intimately familiar with some knowledge domain, even a
common one like what houses look like, it's hard to ignore the bizarre details
made up by an enthusiastic ignoramus. And I don't think you have to be a
carpenter to understand there's something wrong when the protagonist starts
hammering in screws with a wrench.
