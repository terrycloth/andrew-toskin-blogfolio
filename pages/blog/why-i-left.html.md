# Why I Left

My parents made the mistake of telling me to watch Public Television.

Their main concern was sheltering me from exposure to violence --- or worse,
sex! --- through mainstream television. If they couldn't figure out something
else for me to watch, PBS seemed like a safe option. And learning about science
in class at school and then coming home to watch science documentaries taught me
how to think rationally.

The people at Church, though, seemed actively hostile to intellectual inquiry. I
don't think I ever raised any truly subversive questions in Sunday school or
priesthood meetings --- I mean, I guess I don't remember the specifics of any
arguments, but I remember one time everyone had just agreed that theft or murder
or something was *bad*, something as simplistic and trite as that, and I tried
to ask about some hypothetical circumstance which would complicate the issue. So
several times I was tersely dismissed as either impertinent or irrelevant. No
one ever wanted to do anything but read directly out of the official lesson
manual and recite answers we already knew to questions we'd already heard a
million times.

I resigned myself to boredom long before I began to seriously Doubt things. My
very first thoughts of leaving it all behind were probably inspired less by a
crisis of unbelief and more by the tedium of having to quietly swallow ham-sized
morals and then regurgitate everything when prompted.

The crisis of unbelief did come, though.

A lot of Mormons who lose their faith stumble across something --- like
<cite>the Banner of Heaven</cite>, by Jon Krakauer --- some cogent text which
deconstructs the myths and facts of Mormonism, and the evidence against the
Church which grows with each page shatters their world. Funny enough, I didn't
read anything like Krakauer's book until after I'd already decided I was at
least agnostic. It's maybe strange, but part of the reason I broke from the
Church was actually from watching an episode of Criss Angel's <cite>Mind
Freak</cite> TV series. Unlike some of his other stunts, Criss performed this
simple trick, [and then explained it](https://www.youtube.com/watch?v=IRgxMm13_1M).

The psychology behind the trick was so simple: If you tell someone they're going
to feel something, they will. This got me thinking: How can you really know that
"feeling the spirit" is actually divine transdimensional telepathy, and not just
talking to yourself? Grappling with this question was hugely important to me for
most of my last year or two in high school. How can you tell the difference?

You can't. Elohim apparently chose an awfully delicate means of communication ---
human feelings --- one where the signal is so easily lost to noise. In a
religion which so emphasizes not just some nebulous "relationship" with God, but
literal direct communication with him, this was a disastrous realization.

Other questions began to grow as well. The few people I confided in with my
doubts usually ended the conversations with a shrug and invoking the necessity
of faith... but why is faith such a virtue in the first place? In any other
context, believing in something in spite of a lack of evidence, or especially in
spite of *contrary* evidence, would be called insane, or at least irrational,
stupid, unproductive... Why is *belief* such an indesputable principle, why is
faith such an unquestionable end of its own, only when it comes to religion?

All the rest crumbled soon after that.

Each of my older siblings "apostasized" in turn before me, and had big explosive
fights about it with my parents as I was growing up. They scared the shit out of
me when I was eight, and as I turned eighteen I still didn't want to deal with
the drama. So for the last few months of high school, I just bided my time. I
woke up and dressed myself every Sunday morning, but after sacrament, when my
parents went to the Adult classes and weren't around to notice, I went back
outside and sat next to the flowers until the meetings were finished. And after
I graduated from high school, I moved out of my parents' house.

I had serious, uncomfortable, painful doubts about my religion for a couple
years before I could admit to myself that it didn't make sense. But I have never
doubted my doubts since then.


****


I haven't been a believer in years, but reading the CES Letter recently, it
still blows me away to realize what a complete fraud the Church is.

Even worse, though, is the hidden camera footage of the temple endowments. It
struck me just how stupendously, obscenely *boring* the Church is. The endowment
ceremony, one of the most sacred rites in Mormonism, with the poorly acted
videos and overhead audio recordings, nearly the entire process is
machine-automated, and barely requires the participants to be conscious during
the proceedings. I used to feel sorry for people who still believe, but I feel
sorrier for the temple volunteers who have to suffer the monotony of running
through the endowment ceremony over and over.

We have this crazy folklore and mythology, we have this tremendous cultural
heritage, and almost none of it makes it into the official curriculum. The
Church leadership have worked very hard to keep things as dull and safe as
possible.

The Church's teachings don't make a lot of sense if you look too closely --- but
it could have at least been interesting.
