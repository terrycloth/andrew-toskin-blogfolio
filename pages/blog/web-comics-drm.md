The other day, I was talking with a friend about a graphic novel series I've been reading.
I wanted to show them an example of what I was talking about, so I went back to the online comics portal where I originally read the comics, to fetch a particular page.
When I found what I was looking for, I right-clicked to download the image, but saving it wasn't an option in the context menu.

Many image-hosting sites disable right-click downloads, or even disable the context menu altogether, but that's not too hard to work around if you know HTML.
So I launched Firefox's DOM inspector tool on the page and drilled down, looking for the buried `<img />` tag.
I couldn't find one.

Many image-hosting sites go a step further with z-axis layered `<div>` elements --- specifically, it seems, to try to foil people who use their browser's built-in DOM inspector.
The "Pick element from this page" tool selects what is basically a bunch of empty `<div>` blocks because those are what's on the "surface" when you click on the picture.
If you poke around outside the empty blocks, the actual `<img />` tag turns out to be hidden somewhat earlier or later in the document tree.
I've encountered this enough times too that I can usually find the image element I want without too much trouble.
Except that's not what was happening here either.

Instead they used a `<canvas>` element and the related Web APIs to _dynamically render_ the image's raster data.
In fact, they used _multiple_ canvas elements, each holding a different zigzagging slice of the full picture, which is only visible as a coherent whole when the Web application combines the different pieces for you.

Instead of merely obfuscating the image element (and its `src` URL attribute), they obfuscated the image data itself.
This defeats not only HTML-literate readers, but also scripts which might try to scrape all parseable URLs for download.
Even a canvas-to-base64 conversion would only produce a fraction of the image, with no simple way of stitching it back together with the other pieces.

I marveled both at the ingenuity on display and also the wasted engineering effort...
Because I still wanted a copy of this comic book page for reference in that conversation I was in the middle of.
So I just took a screenshot of the page instead.
