# The Problem With Chat Applications

When I talk to my friends, I just want to be able to stroll down the street,
stick my head in the door, and say hello. But everyone I know lives in one or
more gated communities.

There are many who already make good arguments about
[why we should decentralize the Web](https://www.decentralizedweb.net/),
but I don't really see anyone talking about how internet chat faces a lot of the
same problems: The big chat services compete more on social momentum and inertia
than on quality or features, so everything is the same, but at the same time
nothing is interchangeable; hardly anyone takes security and privacy seriously
enough; and users have so little control over their own data.



## Nothing is feature-complete

### Visual language

In the earlier days of chat I used to look down on emoticons, but came to
appreciate that they're useful for adding tone to informal text communication.
It's part of our Internet dialect, and for users who really like them, it's
beginning to creep into the way we think.

Now, it occurs to me that emoji being added to the Unicode standard is sorta
problematic, because different fonts will draw the emoji differently in ways
that might change the meaning. Consider the birthday cake, which in some fonts
is a full cake, and in some just a single slice. For the latter someone might
write "They started my own birthday party without me! By the time I got there,
all that was left was one skinny little 🎂". The message would seem odd if the
recipient's chat application used the full-cake version of the emoji. Likewise,
certain smiley faces seem to have a slightly different expression, or are
exaggerated to different degrees, in the different fonts. Changing the font does
not usually change the meaning of regular text like this.

Stickers basically solve this problem, as the picture is an actual static image
file, which should render the same on any client compatible with the service.
The problems then are the familiar ones: interoperability and user control.

Text can be selected and copied, while sticker packs are usually integrated in
the chat service in a way that allows forwarding within the client but not
sending to people in another service.

For most chat services, the creation of stickers, like the creation of emoji, is
also a top-down process, which is not good for the evolution of our visual
language. Some chat services will display the image when a user posts a web URL
that resolves to an image file, and this is great. But it's much faster to pick
from a predefined collection of images like a sticker pack than to search the
Web for something appropriate, and speed is an important feature in real-time
communication. So users need a clear and simple way to create their own sticker
packs, and to use the stickers created by other users.


### Rich text


### Drafts


### Threads?


### Message revision and retraction



## Security vs usability

open source and end-to-end encrypted with forward secrecy, so we can trust it, but allowing synchronization for all devices you use, and preserving the message archive.

currently leaning toward Telegram and Signal, as these have the most momentum, but there are others...



## Federation
