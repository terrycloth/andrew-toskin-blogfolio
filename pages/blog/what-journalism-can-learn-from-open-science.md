# Journalism Could Learn A Lot From The Open Science Movement (and from science in general)

open science is often billed as a push for greater collaboration (and I
guess transparency to potential readers?), but it's also about
recognizing a greater need for verification, which is why they don't
just want freely publicly accessible journals but also public original
data.

Like in open science, infographics and data-driven reporting should be
published along with the source data.

Journalists already do a fair bit of the collaboration which Open
Science aims to get better at, which is good. Building on top of the
story started by other reporters is good, linking each other's work is
good. But like the scientific community is realizing, journalists maybe
ought to spend some portion of their efforts on verification, fact
checking each other's work, creating a sort of peer review process...
Protecting the anonymity of whistleblowers makes that tricky, but you
could at least maybe research If A Then B type questions to make sure
the claims hold up.

And then, if some report fails verification, it's important for people
to know about it: News sites could develop web technologies that
automatically notify readers and downstream authors of an article of
retractions and corrections. Downstream authors could then also make any
necessary retractions or corrections, and automatically notify their
readers and downstreams.

Notifying readers and funding this more careful sort of reporting would
of course be easier if there were a renewed emphasis on subscriptions.
And like science maybe applying for grants.
