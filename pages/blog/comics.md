No other art form has to stop and think about what to call itself as much as comics.

Well, okay, maybe video games, if you're one of those people who prefer to talk about "interactive digital experiences."

I mean, "comics" is certainly the defacto standard by a significant margin. But it's unfortunate that the word also denotes humor. On the one hand, languages evolve inevitably, and words can change meaning, but on the other hand, we still use <i>comic</i> to describe comedies and refer to comedians. This is probably the reason some people still expect all comics to be like newspaper cartoons.

The alternative names aren't great, though. <i>Graphic novel</i> maybe relies too much on comparison to prose novels, and people unacquainted with the term are likely to interpret "graphic" as a euphemism for depictions explicit sex acts or extreme violence. I rather like <i>fumetti</i> the Italian word for comics. It comes from <i lang="it">fumetto</i>, meaning smoke, a reference to the way words for dialog were drawn in the earliest works --- as apt a metonym for the art form as any. However, in English, <i>fumetti</i> tends to be used specifically for photo comics, when it's used at all.
