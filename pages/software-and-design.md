# a reminder for myself as much as anything

**Make it work.** You have to be pragmatic. Above all else, software is
generally supposed to *do* something. Go with whatever more effectively and
efficiently moves us toward our goal.

**Make it well.** Good design and clear, correct code are important, practical
values too --- because you never make it just once and then you're done. You have
to keep remaking it, *someone* keeps tinkering with it, and this work is
complicated enough without tying yourself into knots with duct-tape/spaghetti
solutions. All but the most trivial and private of projects should have
documentation for both the users and developers. A perfect design would need no
explanation --- but unless you've created the ultimate empathic, telepathic
non-interface, it's not perfect, and you have no excuse. Also, know that
feature-richness, configurability, and usability are orthogonal. Ideally, your
software should rank highly in all three, but if there is ever a trade-off,
usability takes precedence.

**Make it beautiful.** (That goes for the code *and* the UI.) We spend a lot of
time staring at computer screens; don't make people suffer through your ugly
design. Interface design should be part of your plans for the project from the
beginning.

**Make it your own.** If you want everyone to like you, no one will love you. By
all means, cast the widest net you can, but it's better to find and stick to a
clear vision, to follow your intended audience and use-case, than to try to
pander to every person on the planet.
