/*
 * This script dynamically applies inline styles for interlinear glossing. Each
 * layer of the glossing set is marked up as a paragraph, with span elements
 * marking up the corresponding substrings which then get styled so they line up
 * vertically, allowing readers to see the word-for-word notes.
 */

// Make a list of all examples in the current page.
var examples = document.getElementsByClassName( "example-contents" );

var interlinearGloss = function () {
	// Applying font family designated in the CSS @font-face rules seems to take
	// some time in most browsers. Wait a second, so we can calculate the
	// correct font sizes and so on.
	window.setTimeout( function () {
		// Set up glossing for each example.
		for ( var i = 0; i < examples.length; i++ ) {
			// Make a list of all substrings in the Lojban part of the current example.
			var jboStrings = examples[ i ].getElementsByClassName( "example-jbo-substring" );
			// Make a list of all substrings in the glossed part of the current example.
			var glossStrings = examples[ i ].getElementsByClassName( "example-gloss-substring" );

			// TODO: Maybe do initial padding and borders here in the JS, so there's no
			// padding or borders for the paragraphs when users read with JS disabled.

			// Find the minimum padding for expanding substrings in the example text.
			// The regular expression chops off "px" from the returned string.
			var cssPadding = window.getComputedStyle( jboStrings[ 0 ]).paddingLeft;
			var trimPx = /(\d*\.?\d*)px/;
			var minimumPadding = Number( cssPadding.replace( trimPx, "$1" ));
			// Which substring needs padding.
			var needsPadding;
			// Extra padding needed to vertically align corresponding substrings in jbo
			// and gloss (to be determined in the next loop).
			var extraPadding;

			// Note: The Lojban and gloss must have the same number of substrings (the same number of span elements)...
			for ( var j = 0; j < jboStrings.length; j++ ) {
				// Find out which substring needs padding.
				if ( jboStrings[ j ].offsetWidth < glossStrings[ j ].offsetWidth ) {
					needsPadding = jboStrings[ j ];
				}
				else {
					needsPadding = glossStrings[ j ];
				}

				// Calculate the amount of padding needed.
				extraPadding = Math.abs( jboStrings[ j ].offsetWidth - glossStrings[ j ].offsetWidth );
				// Add the padding.
				needsPadding.style.paddingRight = extraPadding + minimumPadding + "px";
			}

			// Container blocks for the lojban and gloss parts of the full example.
			var jboBlock = examples[ i ].getElementsByClassName( "example-jbo" )[ 0 ];
			var glossBlock = examples[ i ].getElementsByClassName( "example-gloss" )[ 0 ];

			// .style.lineHeight for both should always be 3.5 unless we add more layers of glossing.
			// Admittedly, this is a bit of a "magic number"... This may have to adjust based on the
			// actual fonts used and number of layers of glossing.
			jboBlock.style.lineHeight = "3.5";
			glossBlock.style.lineHeight = "3.5";

			// Another (magic) number: slight adjustment for positioning the gloss paragraph.
			var cssFontSize = window.getComputedStyle( examples[ i ]).fontSize;
			var positionAdjustment = Number( cssFontSize.replace( trimPx, "$1" )) / 3;

			// Relative positioning of the gloss paragraph should be (approximately) the same as the height of the jbo paragraph.
			glossBlock.style.position = "relative";
			glossBlock.style.top = "-" + ( jboBlock.offsetHeight - positionAdjustment ) + "px";
			glossBlock.style.height = "0";
		}
	}, 1000 );
};

// Now actually apply interlinear gloss styles.
interlinearGloss();

// Rerun interlinear gloss styles if the window resizes.
window.addEventListener( "resize", function() {
	// Need to undo styles set above before rerunning.
	for ( var i = 0; i < examples.length; i++ ) {
		var jboStrings = examples[ i ].getElementsByClassName( "example-jbo-substring" );
		var glossStrings = examples[ i ].getElementsByClassName( "example-gloss-substring" );
		var jboBlock = examples[ i ].getElementsByClassName( "example-jbo" )[ 0 ];
		var glossBlock = examples[ i ].getElementsByClassName( "example-gloss" )[ 0 ];

		for ( var j = 0; j < jboStrings.length; j++ ) {
			jboStrings[ j ].style.paddingRight = "";
			glossStrings[ j ].style.paddingRight = "";
		}
		jboBlock.style.lineHeight = "";
		glossBlock.style.lineHeight = "";
		glossBlock.style.position = "";
		glossBlock.style.top = "";
		glossBlock.style.height = "";
	}

	interlinearGloss();
}, false );
