@charset "UTF-8"

/*
 * "PT Sans" font family developed by ParaType <http://www.paratype.com/>.
 * Web fonts generated by Font Squirrel <http://www.fontsquirrel.com> on February 8, 2016.
 */

@font-face
	font-family: "PT Sans"
	src: url( "/fonts/pt-sans/pt_sans-web-bold.eot" )
	src: url( "/fonts/pt-sans/pt_sans-web-bold.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-sans/pt_sans-web-bold.woff2" ) format( "woff2" ),
		url( "/fonts/pt-sans/pt_sans-web-bold.woff" ) format( "woff" ),
		url( "/fonts/pt-sans/pt_sans-web-bold.ttf" ) format( "truetype" )
	font-weight: bold
	font-style: normal


@font-face
	font-family: "PT Sans"
	src: url( "/fonts/pt-sans/pt_sans-web-bolditalic.eot" )
	src: url( "/fonts/pt-sans/pt_sans-web-bolditalic.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-sans/pt_sans-web-bolditalic.woff2" ) format( "woff2" ),
		url( "/fonts/pt-sans/pt_sans-web-bolditalic.woff" ) format( "woff" ),
		url( "/fonts/pt-sans/pt_sans-web-bolditalic.ttf" ) format( "truetype" )
	font-weight: bold
	font-style: italic


@font-face
	font-family: "PT Sans"
	src: url( "/fonts/pt-sans/pt_sans-web-italic.eot" )
	src: url( "/fonts/pt-sans/pt_sans-web-italic.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-sans/pt_sans-web-italic.woff2" ) format( "woff2" ),
		url( "/fonts/pt-sans/pt_sans-web-italic.woff" ) format( "woff" ),
		url( "/fonts/pt-sans/pt_sans-web-italic.ttf" ) format( "truetype" )
	font-weight: normal
	font-style: italic


@font-face
	font-family: "PT Sans"
	src: url( "/fonts/pt-sans/pt_sans-web-regular.eot" )
	src: url( "/fonts/pt-sans/pt_sans-web-regular.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-sans/pt_sans-web-regular.woff2" ) format( "woff2" ),
		url( "/fonts/pt-sans/pt_sans-web-regular.woff" ) format( "woff" ),
		url( "/fonts/pt-sans/pt_sans-web-regular.ttf" ) format( "truetype" )
	font-weight: normal
	font-style: normal


/*
 * "PT Serif" font family devoloped by ParaType <http://www.paratype.com/>.
 * Web fonts generated by Font Squirrel ( http://www.fontsquirrel.com ) on February 8, 2016.
 */

@font-face
	font-family: "PT Serif"
	src: url( "/fonts/pt-serif/pt_serif-web-bold.eot" )
	src: url( "/fonts/pt-serif/pt_serif-web-bold.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-serif/pt_serif-web-bold.woff2" ) format( "woff2" ),
		url( "/fonts/pt-serif/pt_serif-web-bold.woff" ) format( "woff" ),
		url( "/fonts/pt-serif/pt_serif-web-bold.ttf" ) format( "truetype" )
	font-weight: bold
	font-style: normal


@font-face
	font-family: "PT Serif"
	src: url( "/fonts/pt-serif/pt_serif-web-bolditalic.eot" )
	src: url( "/fonts/pt-serif/pt_serif-web-bolditalic.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-serif/pt_serif-web-bolditalic.woff2" ) format( "woff2" ),
		url( "/fonts/pt-serif/pt_serif-web-bolditalic.woff" ) format( "woff" ),
		url( "/fonts/pt-serif/pt_serif-web-bolditalic.ttf" ) format( "truetype" )
	font-weight: bold
	font-style: italic


@font-face
	font-family: "PT Serif"
	src: url( "/fonts/pt-serif/pt_serif-web-italic.eot" )
	src: url( "/fonts/pt-serif/pt_serif-web-italic.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-serif/pt_serif-web-italic.woff2" ) format( "woff2" ),
		url( "/fonts/pt-serif/pt_serif-web-italic.woff" ) format( "woff" ),
		url( "/fonts/pt-serif/pt_serif-web-italic.ttf" ) format( "truetype" )
	font-weight: normal
	font-style: italic


@font-face
	font-family: "PT Serif"
	src: url( "/fonts/pt-serif/pt_serif-web-regular.eot" )
	src: url( "/fonts/pt-serif/pt_serif-web-regular.eot?#iefix" ) format( "embedded-opentype" ),
		url( "/fonts/pt-serif/pt_serif-web-regular.woff2" ) format( "woff2" ),
		url( "/fonts/pt-serif/pt_serif-web-regular.woff" ) format( "woff" ),
		url( "/fonts/pt-serif/pt_serif-web-regular.ttf" ) format( "truetype" )
	font-weight: normal
	font-style: normal



/*
 * I'm in the middle of rethinking and rebuilding my site's templates and styles,
 * so pardon my cruft.
 */

body
	color: hsl( 0, 0%, 15% )
	background: hsl( 0, 0%, 97% )
	font-family: "PT Sans", sans-serif
	line-height: 1.2em
	text-rendering: optimizeLegibility
	padding: 1em
	max-width: 40em
	margin: auto

h1, h2, h3, h4, h5, h6
	font-family: "PT Serif", serif
	font-weight: normal
	line-height: 110%

header
	text-align: center

h1
	font-size: 200%
	text-align: center
	max-width: 75%
	margin: auto

h2
	font-size: 160%
	margin: 2em auto 1em auto

h3
	font-size: 140%
	margin: 1em auto

h4
	font-size: 120%

h5
	font-size: 115%

h6
	font-size: 105%

img
	max-width: 100%
	max-height: 100%

img.portrait
	// approximately the width of my name (depends on font).
	width: 13em

address
	display: block
	font-size: 130%


#networks
	list-style: none
	margin: auto
	padding: 0

#networks > li
	display: inline

#networks img
		width: 1.5em

p.salutations
	font-size: 150%
	text-align: center


.project-title
	font-size: 120%


.project-thumbnail
	margin: auto


.project-thumbnail img
	max-height: 8em


a
	color: blue
	text-decoration: none

a:hover
	color: green
	text-decoration: underline

a:active
	color: red

a:visited
	color: hsl( 308, 80%, 24% )


figure
	margin: 1em auto
	text-align: center


#chromosome
	max-width: 4em



@media ( max-width:10in )
	body
		font-size: 0.16in


	.project-lists
		max-width: 40em



@media ( min-width:10.01in )
	body
		font-size: 0.18in


	#projects
		max-width: 90em
		margin: 2em auto


	.project-lists
		width: 45%
		margin: auto


	#storytelling
		float: left
		text-align: right


	#code
		float: right
		text-align: left

	#design
		clear: both
		float: left
		text-align: right

	#music
		float: right
		text-align: left

	#older
		clear: both
		float: left




@media ( min-width:16in )


@media ( min-width:24in )
	body
		font-size: 0.22in




@media print
	body
		color: black
		background: white
		font-size: 11pt


	a
		color: inherit
		font: inherit
		text-decoration: inherit

	a:after
		content: " <" attr( href ) ">"


@page
